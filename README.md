# checkUsernameAvailability

Sepia's backend consists of serverless functions, MongoDB, and object storage. The object storage is hosted on Backblaze B2, and all requests to the API are handled by Cloudflare workers. When necessary, information is passed on to IBM cloud functions which can interface with the database.

This worker handles requests with the /checkUsernameAvailability endpoint. It accepts basic authentication in the form of a username which is passed on to the "checkUsernameAvailability" IBM cloud function. The IBM cloud function returns a boolean that determines if the username is available or not

## Deployment

Right now there is no deployment pipeline configuration setup. To deploy this yourself you either create a Cloudflare Worker and manually paste in the contents of index.js or configure [wrangler](https://developers.cloudflare.com/workers/cli-wrangler/install-update), Cloudflare's cli tool for workers.
